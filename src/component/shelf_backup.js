import React, {Component} from 'react';
import Book_shelf_changer from './book_shelf_changer';






class Shelf extends Component {
  constructor(){
    super();
 this.state = {
    books: [],
   
  }
}


  render() {
    var scope = this;    
    const {books } = this.state;
   var shelf= books.map((book, index) => {
     return book.shelf

    });


    return (
          <div className="bookshelf">
                  <h2 className="bookshelf-title">{this.props.title}</h2>
              
                  <div className="bookshelf-books">
                    <ol className="books-grid">
                    
                    {this.props.bookList.map(booksInbookList=>{
                          return (<li>
                            <div className="book">
                              <div className="book-top">
                             {/* { (e.imageLinks!==undefined)?(<div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url('+e.imageLinks+')' }}></div>
                              ):(<div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url("http://bit.ly/2t9rSY0")' }}></div>)
                               }   */}
                               {/* {console.log(e.imageLinks)} */}
                              <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url('+booksInbookList.imageLinks.smallThumbnail+')' }}></div>
                                <Book_shelf_changer  book={booksInbookList} changeShelf={scope.props.changeShelf}/>   
                               </div>
                              
                              <div className="book-title">{booksInbookList.title}</div>
                              <div className="book-authors">{booksInbookList.authors}</div>
                            </div>
                          </li>)
                          })}
                      </ol>
                    </div>
                 </div>     
    );
  }
}
export default Shelf;