import React, {Component} from 'react';
// import '../CSS/Home.css'
import {getAll} from '../BooksAPI.js'
import {update} from '../BooksAPI.js'
import Shelf from './shelf'
import Header from './header.js'
// import Shelf from './Shelf.js'
// import {getAll}  from './BooksAPI.js'
import OpenSearch from './open_search.js'
import Search from './search.js'


 class Home extends React.Component {
    
    constructor() {
        super();
        this.state = {
            currentlyReading: [],
            toRead: [],
            read: [],
            allBooks:[],
        }
    }


    componentDidMount() {
        getAll()
            .then((books) => {
                this.shelf_update(books)
        });
    }

     shelf_update(books){
        let currentlyReading = books.filter((book) => {return book.shelf === 'currentlyReading'});
        let toRead = books.filter((book) => {return book.shelf === 'wantToRead'});
        let read = books.filter((book) => {return book.shelf === 'read'});
        this.setState({
            currentlyReading: currentlyReading,
            toRead: toRead,
            read: read,
            allBooks:books,
        })

    }

    search_id_function(books,input){
        this.state.allBooks.forEach(function(element,index) {
            if(element['id']===books.id)
            {
                    books.shelf=input;
            }
        });
        }
       
    changeShelf(books,input) {
            update(books,input).then(()=> {
            
                if(input==="currentlyReading"){
                        this.search_id_function(books,input)
                }
                if(input==="wantToRead"){
                    this.search_id_function(books,input)
                }
                if(input==="read"){
                    this.search_id_function(books,input)
                }
                if(input==="none"){
                    this.search_id_function(books,input)
                }
            this.shelf_update(this.state.allBooks)
                });
    }


    render() {
        const element = <div className="list-books">
                            <Header />
                            <div className='list-books-content'>
                                {/* <Search allBooks={this.state.allBooks}/> */}
                                <Shelf title='Curently Reading' bookList={this.state.currentlyReading } changeShelf={this.changeShelf.bind(this)} />
                                <Shelf title='Want To Read' bookList={this.state.toRead} changeShelf={this.changeShelf.bind(this)}/>
                                <Shelf title='Read' bookList={this.state.read} changeShelf={this.changeShelf.bind(this)}/>
                                {/* <Shelf title='books' bookList={this.props.book} changeShelf={this.changeShelf.bind(this)}/> */}
                            </div>
                            <OpenSearch />
                        </div>
        return element
    }
}
export default Home