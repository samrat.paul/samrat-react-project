import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';   
import '../App.css';
import {search} from '../BooksAPI.js'

import Book_shelf_changer from './book_shelf_changer';
import {update} from '../BooksAPI.js'
import {getAll} from '../BooksAPI.js'
import {debounce} from "lodash";



export default class Search extends Component {
    constructor(props){
        super(props);
        this.state={
            currentlyReading: [],
            toRead: [],
            read: [],
            allBooks:[],
            allbookofsearch:[],
            message: 'Enter book title to search',
            img:null,
        }
        // this.search_book=this.search_book.bind(this)
        this.searchBook = this.searchBook.bind(this)
        
    }

componentDidMount() {
    getAll()
        .then((books) => {
            this.shelf_update(books)
    });
}

search_book = debounce(this.searchBook,300);

shelf_update(books){
    let currentlyReading = books.filter((book) => {return book.shelf === 'currentlyReading'});
    let toRead = books.filter((book) => {return book.shelf === 'wantToRead'});
    let read = books.filter((book) => {return book.shelf === 'read'});
    this.setState({
        currentlyReading: currentlyReading,
        toRead: toRead,
        read: read,
        allBooks:books,
    })

}
search_id_function(books,input){
    this.state.allbookofsearch.forEach(function(element,index) {
        if(element['id']===books.id)
        {
                books.shelf=input;
        }
    });
    }

   
changeShelf(books,input) {
        update(books,input).then(()=> {
        
            if(input==="currentlyReading"){
                    this.search_id_function(books,input)
            }
            if(input==="wantToRead"){
                this.search_id_function(books,input)
            }
            if(input==="read"){
                this.search_id_function(books,input)
            }
        this.shelf_update(this.state.allBooks)
            });
}

 searchBook(value) {
                    if(value === "")
                    this.setState({allbookofsearch: [], message: 'Enter book title to search'})
                    else {
                            search(value)
                            .then((booksInSearch) => {
                                booksInSearch.forEach((book) => {
                                    this.state.allBooks.forEach((bookInList) => {
                                        if(book.id === bookInList.id)
                                         {
                                            book.shelf = (bookInList.shelf)
                                            // book.imageLinks=('url('+bookInList.imageLinks+')')?('url('+bookInList.imageLinks+')'):'none'
                                         }
                                        }) 
                                        book.shelf = (book.shelf) ? (book.shelf) : 'none'
                                    });
                                this.setState({
                                    allbookofsearch:booksInSearch,
                                    message: 'Found ' + booksInSearch.length + ' Books'
                                });            
                            })
                            .catch(() => {
                                this.setState({allbookofsearch: [], message: 'Sorry!!! books not found',img:'url("http://bit.ly/2t9rSY0")'})
                            })
                        }
                
                 }


    render() {
        var scope = this;   
        return (<div className = "search-books">
                    <div className = "search-books-bar"> 
                    <NavLink to = "/" className = "close-search"></NavLink>
                          <div className = "search-books-input-wrapper">
                             <input type="text" onKeyUp={(event)=>{this.search_book(event.target.value)}} placeholder="Search by title or author"/>
                        </div> 
                        </div> 
                    
                    <div className = "search-books-results">
                    <div className="search-message">{this.state.message}</div>
                        <ol className="books-grid">
                         
                         {this.state.allbookofsearch.map(e=>{
                          return (
                          <li>
                            <div className="book">
                              <div className="book-top">
                               {(e.imageLinks!==undefined)?(<div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url('+e.imageLinks.smallThumbnail+')' }}></div>
                               ):(<div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url("http://bit.ly/2t9rSY0")' }}></div>)
                               }
                                {/* <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url('+e.imageLinks.smallThumbnail+')' }}></div> */}
                                <Book_shelf_changer  book={e} changeShelf={this.changeShelf.bind(this)}/>   
                             </div>
                              
                              <div className="book-title">{e.title}</div>
                              <div className="book-authors">{e.authors}</div>
                            </div>
                          </li>)
                          })}
                         
                        
                        </ol>
                    </div> 
                </div>
    )};
}
