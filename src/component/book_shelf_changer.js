import React from 'react';
const Book_shelf_changer=(props)=>{
  return (
    <div className="book-shelf-changer">
    {/* {props.coverdata} */}
    <select value={props.book.shelf} onChange ={(e)=>props.changeShelf(props.book,e.target.value)}>
      <option value="move" disabled>Move to...</option>
      <option value="currentlyReading">Currently Reading</option>
      <option value="wantToRead">Want to Read</option>
      <option value="read">Read</option>
      <option value="none">None</option>
    </select>
  </div>
  );
}
export default Book_shelf_changer;






