import React, {Component} from 'react';
import Book_shelf_changer from './book_shelf_changer';






class Shelf extends Component {
  constructor(){
    super();
 this.state = {
    books: [],
   
  }
}


  render() {
    var scope = this;    
    // const {books } = this.state;


    return (
          <div className="bookshelf">
                  <h2 className="bookshelf-title">{this.props.title}</h2>
              
                  <div className="bookshelf-books">
                    <ol className="books-grid">
                    
                    {this.props.bookList.map(e=>{
                          return (<li>
                            <div className="book">
                              <div className="book-top">
                                <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url('+e.imageLinks.smallThumbnail+')' }}></div>
                                <Book_shelf_changer  book={e} changeShelf={scope.props.changeShelf}/>   
                               </div>
                              
                              <div className="book-title">{e.title}</div>
                              <div className="book-authors">{e.authors}</div>
                            </div>
                          </li>)
                          })}
                      </ol>
                    </div>
                 </div>     
    );
  }
}
export default Shelf;